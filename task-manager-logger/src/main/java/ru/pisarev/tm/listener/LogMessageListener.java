package ru.pisarev.tm.listener;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.pisarev.tm.api.service.ILoggingService;
import ru.pisarev.tm.dto.LoggerDTO;
import ru.pisarev.tm.service.LoggingService;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import java.io.Serializable;

@AllArgsConstructor
public class LogMessageListener implements MessageListener {

    @NotNull
    final ILoggingService loggingService = new LoggingService();

    @Override
    @SneakyThrows
    public void onMessage(Message message) {
        if (message instanceof ObjectMessage) {
            @NotNull final Serializable entity = ((ObjectMessage) message).getObject();
            if (entity instanceof LoggerDTO)
                loggingService.writeLog((LoggerDTO) entity);
        }
    }

}
