package ru.pisarev.tm.constant;

public interface LogFileName {

    String SESSION_LOG = "log/session.json";

    String USER_LOG = "log/user.json";

    String PROJECT_LOG = "log/project.json";

    String TASK_LOG = "log/task.json";

}
