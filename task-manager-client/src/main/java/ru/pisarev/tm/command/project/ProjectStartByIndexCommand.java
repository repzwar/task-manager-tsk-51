package ru.pisarev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.command.ProjectAbstractCommand;
import ru.pisarev.tm.endpoint.ProjectDto;
import ru.pisarev.tm.exception.entity.ProjectNotFoundException;
import ru.pisarev.tm.util.TerminalUtil;

public class ProjectStartByIndexCommand extends ProjectAbstractCommand {
    @Override
    public String name() {
        return "project-start-by-index";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Start project by index.";
    }

    @Override
    public void execute() {
        System.out.println("Enter index");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @Nullable final ProjectDto project = serviceLocator.getProjectEndpoint().startProjectByIndex(getSession(), index);
        if (project == null) throw new ProjectNotFoundException();
    }
}
