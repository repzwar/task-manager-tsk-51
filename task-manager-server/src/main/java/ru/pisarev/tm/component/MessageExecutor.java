package ru.pisarev.tm.component;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.api.service.IMessageService;
import ru.pisarev.tm.dto.LoggerDTO;
import ru.pisarev.tm.service.MessageService;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MessageExecutor {

    @NotNull
    private static final int THREAD_COUNT = 3;

    @NotNull
    private final IMessageService service = new MessageService();

    @NotNull
    private final ExecutorService es = Executors.newFixedThreadPool(THREAD_COUNT);

    public void sendMessage(@Nullable final Object record,
                            @NotNull final String type) {
        es.submit(() -> {
            @NotNull final LoggerDTO entity = service.prepareMessage(record, type);
            service.sendMessage(entity);
        });
    }

    public void stop() {
        es.shutdown();
    }

}
