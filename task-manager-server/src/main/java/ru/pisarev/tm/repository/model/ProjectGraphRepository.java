package ru.pisarev.tm.repository.model;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.api.repository.model.IProjectRepository;
import ru.pisarev.tm.model.ProjectGraph;

import javax.persistence.EntityManager;
import java.util.List;

public final class ProjectGraphRepository extends AbstractGraphRepository<ProjectGraph> implements IProjectRepository {

    public ProjectGraphRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    public ProjectGraph getReference(@NotNull final String id) {
        return entityManager.getReference(ProjectGraph.class, id);
    }

    public ProjectGraph findById(@Nullable final String id) {
        return entityManager.find(ProjectGraph.class, id);
    }

    public void removeById(@Nullable final String id) {
        ProjectGraph reference = entityManager.getReference(ProjectGraph.class, id);
        entityManager.remove(reference);
    }

    @NotNull
    public List<ProjectGraph> findAll() {
        return entityManager.createQuery("SELECT e FROM ProjectGraph e", ProjectGraph.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    @Override
    public List<ProjectGraph> findAllByUserId(String userId) {
        return entityManager
                .createQuery("SELECT e FROM ProjectGraph e WHERE e.user.id = :userId", ProjectGraph.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public ProjectGraph findByIdUserId(String userId, String id) {
        return getSingleResult(
                entityManager
                        .createQuery("SELECT e FROM ProjectGraph e WHERE e.id = :id AND e.user.id = :userId", ProjectGraph.class)
                        .setHint(QueryHints.HINT_CACHEABLE, true)
                        .setParameter("id", id)
                        .setParameter("userId", userId)
                        .setMaxResults(1)
        );
    }


    @Nullable
    @Override
    public ProjectGraph findByName(@NotNull final String userId, @Nullable final String name) {
        return getSingleResult(
                entityManager
                        .createQuery("SELECT e FROM ProjectGraph e WHERE e.name = :name AND e.user.id = :userId", ProjectGraph.class)
                        .setParameter("name", name)
                        .setParameter("userId", userId)
                        .setMaxResults(1)
        );
    }

    @Nullable
    @Override
    public ProjectGraph findByIndex(@NotNull final String userId, final int index) {
        return getSingleResult(
                entityManager
                        .createQuery("SELECT e FROM ProjectGraph e WHERE e.user.id = :userId", ProjectGraph.class)
                        .setParameter("userId", userId)
                        .setFirstResult(index).setMaxResults(1)
        );
    }

    @Override
    public void removeByName(@NotNull final String userId, @Nullable final String name) {
        entityManager
                .createQuery("DELETE FROM ProjectGraph e WHERE e.name = :name AND e.user.id = :userId")
                .setParameter("userId", userId)
                .setParameter("name", name)
                .executeUpdate();
    }

    @Override
    public void removeByIndex(@NotNull final String userId, final int index) {
        entityManager
                .createQuery("DELETE FROM ProjectGraph e WHERE e.user.id = :userId")
                .setParameter("userId", userId)
                .setFirstResult(index).setMaxResults(1).executeUpdate();
    }

    public void clear() {
        entityManager
                .createQuery("DELETE FROM ProjectGraph e")
                .executeUpdate();
    }

    @Override
    public void clearByUserId(String userId) {
        entityManager
                .createQuery("DELETE FROM ProjectGraph e WHERE e.user.id = :userId")
                .setParameter("userId", userId).executeUpdate();
    }

    @Override
    public void removeByIdUserId(String userId, String id) {
        entityManager
                .createQuery("DELETE FROM ProjectGraph e WHERE e.user.id = :userId AND e.id = :id")
                .setParameter("id", id)
                .setParameter("userId", userId)
                .executeUpdate();
    }

}