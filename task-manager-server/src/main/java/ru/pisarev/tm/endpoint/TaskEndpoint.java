package ru.pisarev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.pisarev.tm.api.service.ServiceLocator;
import ru.pisarev.tm.api.service.dto.IProjectTaskRecordService;
import ru.pisarev.tm.api.service.dto.ITaskRecordService;
import ru.pisarev.tm.api.service.model.ITaskService;
import ru.pisarev.tm.dto.SessionRecord;
import ru.pisarev.tm.dto.TaskRecord;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService
@NoArgsConstructor
public final class TaskEndpoint extends AbstractEndpoint {

    private ITaskRecordService taskRecordService;

    private IProjectTaskRecordService projectTaskService;

    private ITaskService taskService;

    public TaskEndpoint(
            @NotNull final ServiceLocator serviceLocator,
            @NotNull final ITaskRecordService taskRecordService,
            @NotNull final IProjectTaskRecordService projectTaskService,
            @NotNull final ITaskService taskService
    ) {
        super(serviceLocator);
        this.taskRecordService = taskRecordService;
        this.projectTaskService = projectTaskService;
        this.taskService = taskService;
    }

    @WebMethod
    public List<TaskRecord> findTaskAll(@NotNull @WebParam(name = "session") final SessionRecord session) {
        serviceLocator.getSessionRecordService().validate(session);
        return taskRecordService.findAll(session.getUserId());
    }

    @WebMethod
    public void addTaskAll(
            @WebParam(name = "session") final SessionRecord session,
            @WebParam(name = "collection") final Collection<TaskRecord> collection
    ) {
        serviceLocator.getSessionRecordService().validate(session);
        taskRecordService.addAll(session.getUserId(), collection);
    }

    @WebMethod
    public TaskRecord addTask(
            @WebParam(name = "session") final SessionRecord session, @WebParam(name = "task") final TaskRecord entity
    ) {
        serviceLocator.getSessionRecordService().validate(session);
        return taskRecordService.add(session.getUserId(), entity);
    }

    @WebMethod
    public TaskRecord addTaskWithName(
            @WebParam(name = "session") final SessionRecord session,
            @WebParam(name = "name") String name,
            @WebParam(name = "description") String description
    ) {
        serviceLocator.getSessionRecordService().validate(session);
        return taskRecordService.add(session.getUserId(), name, description);
    }

    @WebMethod
    public TaskRecord findTaskById(
            @WebParam(name = "session") final SessionRecord session, @WebParam(name = "id") final String id
    ) {
        serviceLocator.getSessionRecordService().validate(session);
        return taskRecordService.findById(session.getUserId(), id);
    }

    @WebMethod
    public void clearTask(@WebParam(name = "session") final SessionRecord session) {
        serviceLocator.getSessionRecordService().validate(session);
        taskService.clear(session.getUserId());
    }

    @WebMethod
    public void removeTaskById(
            @WebParam(name = "session") final SessionRecord session, @WebParam(name = "id") final String id
    ) {
        serviceLocator.getSessionRecordService().validate(session);
        taskService.removeById(session.getUserId(), id);
    }

    @WebMethod
    public void removeTask(
            @WebParam(name = "session") final SessionRecord session, @WebParam(name = "task") final TaskRecord entity
    ) {
        serviceLocator.getSessionRecordService().validate(session);
        taskRecordService.remove(session.getUserId(), entity);
    }

    @WebMethod
    public TaskRecord findTaskByName(
            @WebParam(name = "session") final SessionRecord session, @WebParam(name = "name") final String name
    ) {
        serviceLocator.getSessionRecordService().validate(session);
        return taskRecordService.findByName(session.getUserId(), name);
    }

    @WebMethod
    public TaskRecord findTaskByIndex(
            @WebParam(name = "session") final SessionRecord session, @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionRecordService().validate(session);
        return taskRecordService.findByIndex(session.getUserId(), index);
    }

    @WebMethod
    public void removeTaskByName(
            @WebParam(name = "session") final SessionRecord session, @WebParam(name = "name") final String name
    ) {
        serviceLocator.getSessionRecordService().validate(session);
        taskService.removeByName(session.getUserId(), name);
    }

    @WebMethod
    public void removeTaskByIndex(
            @WebParam(name = "session") final SessionRecord session, @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionRecordService().validate(session);
        taskService.removeByIndex(session.getUserId(), index);
    }

    @WebMethod
    public TaskRecord updateTaskById(
            @WebParam(name = "session") final SessionRecord session,
            @WebParam(name = "id") final String id,
            @WebParam(name = "name") final String name,
            @WebParam(name = "description") final String description
    ) {
        serviceLocator.getSessionRecordService().validate(session);
        return taskRecordService.updateById(session.getUserId(), id, name, description);
    }

    @WebMethod
    public TaskRecord updateTaskByIndex(
            @WebParam(name = "session") final SessionRecord session,
            @WebParam(name = "index") final Integer index,
            @WebParam(name = "name") final String name,
            @WebParam(name = "description") final String description
    ) {
        serviceLocator.getSessionRecordService().validate(session);
        return taskRecordService.updateByIndex(session.getUserId(), index, name, description);
    }

    @WebMethod
    public TaskRecord startTaskById(
            @WebParam(name = "session") final SessionRecord session, @WebParam(name = "id") final String id
    ) {
        serviceLocator.getSessionRecordService().validate(session);
        return taskRecordService.startById(session.getUserId(), id);
    }

    @WebMethod
    public TaskRecord startTaskByIndex(
            @WebParam(name = "session") final SessionRecord session, @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionRecordService().validate(session);
        return taskRecordService.startByIndex(session.getUserId(), index);
    }

    @WebMethod
    public TaskRecord startTaskByName(
            @WebParam(name = "session") final SessionRecord session, @WebParam(name = "name") final String name
    ) {
        serviceLocator.getSessionRecordService().validate(session);
        return taskRecordService.startByName(session.getUserId(), name);
    }

    @WebMethod
    public TaskRecord finishTaskById(
            @WebParam(name = "session") final SessionRecord session, @WebParam(name = "id") final String id
    ) {
        serviceLocator.getSessionRecordService().validate(session);
        return taskRecordService.finishById(session.getUserId(), id);
    }

    @WebMethod
    public TaskRecord finishTaskByIndex(
            @WebParam(name = "session") final SessionRecord session, @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionRecordService().validate(session);
        return taskRecordService.finishByIndex(session.getUserId(), index);
    }

    @WebMethod
    public TaskRecord finishTaskByName(
            @WebParam(name = "session") final SessionRecord session, @WebParam(name = "name") final String name
    ) {
        serviceLocator.getSessionRecordService().validate(session);
        return taskRecordService.finishByName(session.getUserId(), name);
    }

    @WebMethod
    public List<TaskRecord> findTaskByProjectId(
            @WebParam(name = "session") final SessionRecord session, @WebParam(name = "projectId") final String projectId
    ) {
        serviceLocator.getSessionRecordService().validate(session);
        return projectTaskService.findTaskByProjectId(session.getUserId(), projectId);
    }

    @WebMethod
    public void bindTaskById(
            @WebParam(name = "session") final SessionRecord session,
            @WebParam(name = "taskId") final String taskId,
            @WebParam(name = "projectId") final String projectId
    ) {
        serviceLocator.getSessionRecordService().validate(session);
        projectTaskService.bindTaskById(session.getUserId(), taskId, projectId);
    }

    @WebMethod
    public void unbindTaskById(
            @WebParam(name = "session") final SessionRecord session, @WebParam(name = "taskId") final String taskId
    ) {
        serviceLocator.getSessionRecordService().validate(session);
        projectTaskService.unbindTaskById(session.getUserId(), taskId);
    }

}
